/*
 * http://robotfindskitten.org/
 * Leonard Richardson (C) 1997, 2000
 *
 *         r o b o t f i n d s k i t t e n . o r g
 * +-----------------------------------------------------+
 * | [-]                .::. .::.              |\_/|     |
 * | (+)=C              :::::::::              |o o|__   |
 * | | |                ':::::::'              =-*-=__\  |
 * | OOO                  ':::'               c_c__(___) |
 * +-----------------------------------------------------+
 *           -- finding kitten since 1997 --
 *
 *
 * Nouvelle version DOS, (C) Auraes 2020-2023
 * Version 1.1
 * Encodage des caract�res cp850_DOS_Latin1
 * tcc -w -a -K -O -ms rfk.c
 * Turbo C++ Version 3.00 Copyright (c) 1992 Borland International
 *
 */

#include <stdlib.h>
#include <dos.h>
#include <time.h>

typedef unsigned char byte;
typedef unsigned short word;

#define MSGS 408

#define LINES 21
#define COLUMNS 80
#define CELLS (LINES*COLUMNS)

#include "rfk_msgs.h"

#define KEYBOARD_INT 0x16
#define KEY_UP      0x1e
#define KEY_DOWN    0x1f
#define KEY_RIGHT   0x10
#define KEY_LEFT    0x11
#define KEY_ESCAPE  0x1b
#define SCANCODE_UP    0x48
#define SCANCODE_DOWN  0x50
#define SCANCODE_RIGHT 0x4d
#define SCANCODE_LEFT  0x4b
#define TAGLINE "                        -- finding kitten since 1997 --"

/* Clock tick interrupt */
#define INT1C 0x1c

/* inkPaper EQU ink | paper */
#define set_color(a, b) ((a<<8)|(b<<12))

enum COLORS {
   BLACK,
   BLUE,
   GREEN,
   CYAN,
   RED,
   MAGENTA,
   BROWN,
   LIGHTGRAY,

   DARKGRAY,
   LIGHTBLUE,
   LIGHTGREEN,
   LIGHTCYAN,
   LIGHTRED,
   LIGHTMAGENTA,
   YELLOW,
   WHITE
};

/* ICONS */
byte *mrobot =
   "[-]       "
   "(+)=C     "
   "| |       "
   "OOO       ";
byte *mkitten =
   " |\\_/|    "
   " |o o|__  "
   " =-*-=__\\ "
   " C_C(____)";

byte *mkitten_psycho =
   " |\\_/|    "
   "(|) (|)_  "
   " =-O-=__\\ "
   " 3_3(____)";

byte *mheart =
   ".::. .::. "
   "::::::::: "
   "':::::::' "
   "  ':::'   ";

/* COLORS ICONS */
byte *ckitten=
   "\x11\x16\x16\x16\x16\x16\x11\x11\x11\x11"
   "\x11\x16\x12\x11\x12\x16\x16\x16\x11\x11"
   "\x11\x17\x17\x14\x17\x17\x16\x16\x16\x16"
   "\x11\x16\x16\x16\x16\x16\x16\x16\x16\x16";

byte *crobot=
   "\x1e\x12\x1e\x11\x11\x11\x11\x11\x11\x11"
   "\x1e\x14\x1e\x13\x13\x11\x11\x11\x11\x11"
   "\x1e\x11\x1e\x11\x11\x11\x11\x11\x11\x11"
   "\x13\x13\x13\x11\x11\x11\x11\x11\x11\x11";

byte *ckitten_psycho=
   "\x11\x16\x16\x16\x16\x16\x11\x11\x11\x11"
   "\x12\x12\x12\x11\x12\x12\x12\x16\x11\x11"
   "\x11\x17\x17\x14\x17\x17\x16\x16\x16\x16"
   "\x11\x16\x16\x16\x16\x16\x16\x16\x16\x16";

byte *cheart=
   "\x14\x14\x14\x14\x11\x14\x14\x14\x14\x11"
   "\x14\x14\x14\x14\x14\x14\x14\x14\x14\x11"
   "\x14\x14\x14\x14\x14\x14\x14\x14\x14\x11"
   "\x11\x11\x14\x14\x14\x14\x14\x11\x11\x11";

byte *sign[] = {
   "   http://www.robotfindskitten.org",
   "   Leonard Richardson (C) 1997, 2000",
   "   Auraes (C) 2020-2023 (New DOS Edition)",
   "   Your job is to find kitten. This task is complicated by the existance of",
   "   various things which are not kitten. Robot must touch items to determine",
   "   if they are kitten or not.",
   "   The game ends when robot finds kitten. Alternatively, you may end the",
   "   game by hitting the Esc key. See the documentation for more information.",
   "",
   "   Hit any key to start."
};

void interrupt new1c(void);
void interrupt (*old1c)(void);
byte get_key(void);
void clr_scr(void);
void put_str(int y, byte *str);
void put_ch(int x, int y, byte c);
void move_cursor(int dx, int dy);
void instructions(void);
void draw_icon(int x, byte *ptm, byte *ptc);
int available_bios(void);

byte blink_c = '#';
word inkPaper;
volatile word timer;
int robot_x, robot_y, kitten_x, kitten_y;
int errkey, foundkitten, kitten;
int blink = 1;

static void interrupt new1c(void)
{
   ++timer;
   if (blink) {
      if (timer < 3)
         inkPaper = set_color(BLUE, WHITE);
      else
         inkPaper = set_color(WHITE, BLUE);
      put_ch(robot_x, robot_y+4, blink_c);
      timer &= 0x0007;
   }
   (*old1c)();
}

/*
 * available_bios
 *
 * INT 10H 1aH: Set or Query Display Combination Code
 * INT 10H 12H BL=10H: Get EGA Information
 * 03d4h CRT Controller register select
 * return: 3 cga, 2 ega, 1 vga, 0 other
 *
 */
int available_bios(void)
{
   asm {
   /* VGA BIOS available? */
      mov ax,1a00h
      int 10h
      mov bl,al
      mov ax,0001h
      cmp bl,1ah
      je label_ret

   /* EGA BIOS available? */
      mov ah,12h
      mov bl,10h
      int 10h
      mov ax,0002h
      cmp bl,10h
      jne label_ret

   /* CGA BIOS available? */
      mov dx,03d4h
      mov al,0eh
      out dx,al
      inc dx
      in al,dx
      mov ah,al
      mov al,66h /* valeur arbitraire */
      out dx,al
      mov cx,100h
   }
   label_wait:
   asm {
      loop label_wait
      in al,dx
      xchg ah,al
      out dx,al
      mov bh,ah
      mov ax,0003h
      cmp bh,66h
      je label_ret

      xor ax,ax
   }
   label_ret:
   return (_AX);
}

byte get_key(void)
{
   asm {
      mov ah,00h
      int KEYBOARD_INT
      test al,al
      jnz b0
      mov al,KEY_UP
      cmp ah,SCANCODE_UP
      je b0
      mov al,KEY_DOWN
      cmp ah,SCANCODE_DOWN
      je b0
      mov al,KEY_RIGHT
      cmp ah,SCANCODE_RIGHT
      je b0
      mov al,KEY_LEFT
      cmp ah,SCANCODE_LEFT
      je b0
      xor al,al
   }
   b0:
   return (_AL);
}

void clr_scr(void)
{
   inkPaper = set_color(BLUE, BLUE);
   asm {
      mov ax,0b800h
      mov es,ax
      xor di,di
      mov cx,07d0h
      mov ax,inkPaper
      or al,' '
      rep stosw
   }
}

void put_str(int y, byte *str)
{
   asm {
      mov ax,0b800h
      mov es,ax
      mov di,y
      mov cl,07h
      shl di,cl
      mov ax,y
      mov cl,05h
      shl ax,cl
      add di,ax
      mov si,str

      mov cx,COLUMNS
      xor bx,bx
   }
   b1:
   asm {
      mov al,20h
      test bx,bx
      jnz b2
      lodsb
      test al,al
      jnz b2
      inc bx
    }
   b2:
   asm {
      or ax,inkPaper
      stosw
      loop b1
   }
}

void put_ch(int x, int y, byte c)
{
   asm {
      mov ax,0b800h
      mov es,ax
      mov di,y
      mov cl,07h
      shl di,cl
      mov ax,y
      mov cl,05h
      shl ax,cl
      add di,ax
      mov ax,x
      shl ax,01h
      add di,ax
      mov ax,inkPaper
      or al,c
      mov es:[di],ax
   }
}

void move_cursor(int dx, int dy)
{
   byte *pt = msgs[dx+dy*COLUMNS];

   inkPaper = set_color(WHITE, BLUE);
   if (errkey) {
      errkey = 0;
      put_str(2, "");
   }
   if (dx > (COLUMNS-1) || dx < 0 || dy < 0 || dy > (LINES-1)) {
      put_str(2, TAGLINE);
      return;
   }
   if (*pt & 0x80) {
      if ((*pt & 0x7f) == '&')  {
         foundkitten = 1;
         put_str(2, "");
         return;
      }
      *pt &= 0x7f;
      put_str(2, pt);
      *pt |= 0x80;
      return;
   }

   inkPaper = set_color(BLUE, BLUE);
   put_ch(robot_x, robot_y+4, ' ');
   robot_x = dx;
   robot_y = dy;
}

void draw_icon(int x, byte *ptm, byte *ptc)
{
   int i, j;

   for (j=0; j<4; j++) {
      for (i=0; i<10; i++) {
         inkPaper = set_color(*ptc, BLUE);
         put_ch(i+x, j+6, *ptm);
         ++ptm;
         ++ptc;
      }
   }
}

void instructions(void)
{
   int i, j;

   inkPaper = set_color(WHITE, BLUE);
   j = 1;
   for (i=0; i<3; i++) {
      put_str(j, sign[i]);
      ++j;
   }

   draw_icon(23, mrobot, crobot);
   draw_icon(34, mheart, cheart);
   if (random(10) > 2)
      draw_icon(48, mkitten, ckitten);
   else
      draw_icon(48, mkitten_psycho, ckitten_psycho);

   inkPaper = set_color(WHITE, BLUE);
   j = 12;
   for (i=3; i<10; i++) {
      put_str(j, sign[i]);
      ++j;
   }
}

int main(void)
{
   int num, i, dx, dy, n;
   byte c, key, old_video_mode;
   byte *pt = NULL;

/* SAVE_VIDEO_MODE */
   asm mov ah,0fh
   asm int 10h
   asm mov old_video_mode, al

/* SET_VIDEO_MODE */
   asm mov ax,0003h
   asm int 10h

   if (available_bios() == 3) {
      /* NO_BLINK CGA */
      asm {
         mov ax,40h
         mov es,ax
         mov dx,es:[063h]
         add dx,4
         mov al,es:[065h]
         and al,0dfh
         out dx,al
         mov es:[065h],al
      }
   }
   else {
      /* NO_BLINK EGA/VGA */
/*
   Fonctionne pour le EGA avec la SVN de DOSBOX mais pas avec la v0.74-3
*/
      asm mov ax,1003h
      asm xor bl,bl
      asm int 10h
   }

/* NO_CURSOR */
   asm mov ah,01h
   asm mov cx,2020h
   asm int 10h

   randomize();

   *(msgs[MSGS-1]) |= 0x80;
   *(msgs[MSGS-2]) |= 0x80;

   i = MSGS - (10 + random(100)) - 1;

/* Rebattre les messages (sauf Kitten et Robot) */
   n = MSGS-2;
   while(n > i) {
      num = random(n);
      --n;
      pt = msgs[num];
      msgs[num] = msgs[n];
      *pt |= 0x80;
      msgs[n] = pt;
   }

/* Rebattre les cellules */
   n = CELLS;
   while(n) {
      num = random(n);
      --n;
      pt = msgs[num];
      msgs[num] = msgs[n];
      msgs[n] = pt;
   }

   clr_scr();
   instructions();
   get_key();
   clr_scr();

   inkPaper = set_color(CYAN, BLUE);
   for (i=0; i<COLUMNS; i++)
      put_ch(i, 3, '�');

   inkPaper = set_color(BROWN, BLUE);
   put_str(0, "robotfindskitten");
   inkPaper = set_color(GREEN, BLUE);
   put_str(1, "A retro Zen simulation");
   inkPaper = set_color(WHITE, BLUE);
   put_str(2, TAGLINE);

   for (i=0; i<CELLS; i++) {
      pt = msgs[i];
      if ((*pt & 0x7f) == '%') {
         robot_x = i%COLUMNS;
         robot_y = i/COLUMNS;
         *pt &= 0x7f;
         continue;
      }
      if (*pt & 0x80) {
         inkPaper = set_color((random(14)+2), BLUE);
         do {
            c = random(93) + 33;
            if ((*pt & 0x7f) == '&') {
               kitten = c;
               kitten_x = i%COLUMNS;
               kitten_y = i/COLUMNS;
            }
         } while (c == '#');
         put_ch(i%COLUMNS, i/COLUMNS+4, c);
      }
   }

/* REPLACE_INTERRUPT_1C */
   old1c = getvect(INT1C);
   setvect(INT1C, new1c);

   while(1) {
      inkPaper = set_color(BLUE, WHITE);
      put_ch(robot_x, robot_y+4, '#');
      key = get_key();
      blink = 0;
      if (key == KEY_ESCAPE) {
         inkPaper = set_color(WHITE, BLUE);
         put_str(2, "Hit any key to exit.");
         blink = 1;
         robot_x = kitten_x;
         robot_y = kitten_y;
         blink_c = kitten;
         break;
      }
      dx = robot_x;
      dy = robot_y;
      switch (key) {
         case KEY_UP:
            --dy;
            break;
         case KEY_DOWN:
            ++dy;
            break;
         case KEY_LEFT:
            --dx;
            break;
         case KEY_RIGHT:
            ++dx;
            break;
         default:
            errkey = 1;
            inkPaper = set_color(CYAN, BLUE);
            put_str(2, "Invalid command: Use direction keys or Esc.");
            continue;
      }
      move_cursor(dx, dy);
      if (foundkitten) {
           put_ch(robot_x, robot_y+4, ' ');
         for (i=5; i<9; i++) {
            if (key == KEY_LEFT || key == KEY_UP) {
               put_ch(i, 2, kitten);
               put_ch(17-i, 2, '#');
            }
            else {
               put_ch(i, 2, '#');
               put_ch(17-i, 2, kitten);
            }
            timer = 0;
            while (timer < 20);
            put_ch(i, 2, ' ');
            put_ch(17-i, 2, ' ');
         }
         put_str(2, "You found kitten! Way to go, robot!   Hit any key to quit.");
         break;
        }
   }
   while (get_key() != 13);

/* RESTORE_INTERRUPT_1C */
   setvect(INT1C, old1c);

/* RESTORE_VIDEO_MODE */
   asm mov al,old_video_mode
   asm xor ah,ah
   asm int 10h

   return EXIT_SUCCESS;
}

