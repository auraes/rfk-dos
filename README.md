# robotfindskitten : Une simulation Zen
http://www.robotfindskitten.org  
Leonard Richardson (C) 1997, 2000  
Auraes (C) 2020-2023 (nouvelle version DOS)  
Version 1.1

Ce jeu a été écrit à l’origine, par Leonard Richardson, pour le concours Nerth Pork robotfindskitten.
Je n’ai pas cherché à reproduire le code source, il n’est donc pas conforme à l’original.

De nombreuses informations sur robotfindskitten sont disponibles sur les sites :  
http://www.robotfindskitten.org.  
https://www.crummy.com/software/robotfindskitten  
https://gitlab.com/DavidGriffith/rfk-inform  

Dans ce jeu, vous êtes le Robot (#). Votre objectif consiste à trouver le chaton. Cette tâche est compliquée par l’existence de diverses choses qui ne sont pas des chatons. Le robot doit toucher les objets pour déterminer s’ils sont chatons ou non. Déplacez le robot à l’aide des touches curseur du clavier numérique. Le jeu se termine lorsque le robot trouve le chaton.
Vous pouvez également terminer le jeu en appuyant sur la touche echap (Esc).
